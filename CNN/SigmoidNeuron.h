#pragma once

#define RAND_MAX_SN 1.00
#define RAND_MIN_SN -1.00
#include <iostream>
#include <malloc.h>

class SigmoidNeuron
{
private:
	float* weights;
	int nrOfInputs;
	float bias;
	float weightedInput;
	float sigma(float value)
	{
		return (1 / (1 + exp(-value)));
	}
public:
	float getWeightedInput()
	{
		return weightedInput;
	}
	void setNrOfInputs(int nrOfInputs)
	{
		this->nrOfInputs = nrOfInputs;
		weights = (float*)malloc(nrOfInputs * sizeof(float));
	}
	void updateWeigthsRandomly()
	{
		for (int i = 0; i < nrOfInputs; i++)
		{
			weights[i] = ((float)rand() / (RAND_MAX/2)) - 1;
		}
		bias = ((float)rand() / (RAND_MAX / 2)) - 1;
	}
	void toString()
	{
		std::cout << "Weights: ";
		for (int i = 0; i < nrOfInputs; i++)
		{
			std::cout << weights[i] << ", ";
		}
		std::cout << " Bias: " << bias << "\n";		
	}

	float calculateWeightetInput(float* input)
	{
		float sum = 0;
		for (int i = 0; i < nrOfInputs; i++)
		{
			sum += weights[i] * input[i];
		}
		this->weightedInput = (sum + bias);
		return (sum + bias);
	}

	float calculateActivation(float* input)
	{
		return (sigma(-calculateWeightetInput(input)));
	}
};

