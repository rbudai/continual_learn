#pragma once
#include <stdlib.h>
#include <cmath>
class CostFunction {
private: 
	int nbOfInputs;
	float* obtainedOutput;
	float* trueOutput;
	int nbOfTrainingItems;

public:
	void setNbOfInputs(int nb)
	{
		this->nbOfInputs = nb;
		obtainedOutput = (float*)malloc(nbOfInputs * sizeof(float));
		trueOutput = (float*)malloc(nbOfInputs * sizeof(float));
	}
	void setNbOfTrainingItems(int n)
	{
		this->nbOfTrainingItems = n;
	}

	void setTrueOutput(float* trueOutput)
	{
		this->trueOutput = trueOutput;
	}
	void setObtainedOutput(float* obtainedOutput)
	{
		this->obtainedOutput = obtainedOutput;
	}
	float crossEntropyFunction() {
		float sum = 0;
		for (int i = 0;i < nbOfInputs;i++)
		{
			sum += trueOutput[i] * log(obtainedOutput[i]) + (1 - trueOutput[i]) * log(1 - obtainedOutput[i]);
		}
		return (sum);
	}
	float crossEntropyFunctionDerivative(int index)
	{

	}
};