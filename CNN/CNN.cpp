// CNN.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <malloc.h>
using namespace std;
#define RAND_MAX_SN 1.00
#define RAND_MIN_SN -1.00

class SigmoidNeuron
{
private:
	float* weights;
	int nrOfInputs;
	float bias;
	float weightedInput;
	float sigma(float value)
	{
		return (1 / (1 + exp(-value)));
	}
public:
	float getWeightedInput()
	{
		return weightedInput;
	}
	void setNrOfInputs(int nrOfInputs)
	{
		this->nrOfInputs = nrOfInputs;
		weights = (float*)malloc(nrOfInputs * sizeof(float));
	}
	void updateWeigthsRandomly()
	{
		for (int i = 0; i < nrOfInputs; i++)
		{
			weights[i] = ((float)rand() / (RAND_MAX / 2)) - 1;
		}
		bias = ((float)rand() / (RAND_MAX / 2)) - 1;
	}
	void toString()
	{
		std::cout << "Weights: ";
		for (int i = 0; i < nrOfInputs; i++)
		{
			std::cout << weights[i] << ", ";
		}
		std::cout << " Bias: " << bias << "\n";
	}

	float calculateWeightetInput(float* input)
	{
		float sum = 0;
		for (int i = 0; i < nrOfInputs; i++)
		{
			sum += weights[i] * input[i];
		}
		this->weightedInput = (sum + bias);
		return (sum + bias);
	}

	float calculateActivation(float* input)
	{
		return (sigma(-calculateWeightetInput(input)));
	}
};


class FullyConnectedLayer
{
private:
	SigmoidNeuron* list;

	int nrOfNeurons;
	int nrOfInputs;
public:
	FullyConnectedLayer() {}
	FullyConnectedLayer(int nrOfNeurons, int nrOfInputs)
	{
		this->nrOfNeurons = nrOfNeurons;
		this->nrOfInputs = nrOfInputs;
		list = (SigmoidNeuron*)malloc(nrOfNeurons * sizeof(SigmoidNeuron));
		for (int i = 0; i < nrOfNeurons; i++)
		{
			list[i].setNrOfInputs(this->nrOfInputs);
			list[i].updateWeigthsRandomly();
		}
	}
	void DeleteLayer()
	{
		free(list);
	}
	SigmoidNeuron getNeuron(int index)
	{
		return(list[index]);
	}
	void printNeurons()
	{
		std::cout << "layer: \n";
		for (int i = 0; i < nrOfNeurons; i++)
		{
			std::cout << "Neuron: " << i << " ";
			list[i].toString();

		}
	}
	int getNrOfInputs()
	{
		return nrOfInputs;
	}
	int getNrOfNeurons()
	{
		return nrOfNeurons;
	}
	float* calculateOutput(float* input)
	{
		float* output;
		output = (float*)malloc(nrOfNeurons * sizeof(float));
		for (int i = 0;i < nrOfNeurons;i++)
		{
			output[i] = list[i].calculateActivation(input);
		}
		return(output);
	}
};


class CostFunction {
private:
	int nbOfInputs;
	float* obtainedOutput;
	float* trueOutput;
	int nbOfTrainingItems;

public:
	void setNbOfInputs(int nb)
	{
		this->nbOfInputs = nb;
		obtainedOutput = (float*)malloc(nbOfInputs * sizeof(float));
		trueOutput = (float*)malloc(nbOfInputs * sizeof(float));
	}
	void setNbOfTrainingItems(int n)
	{
		this->nbOfTrainingItems = n;
	}

	void setTrueOutput(float* trueOutput)
	{
		this->trueOutput = trueOutput;
	}
	void setObtainedOutput(float* obtainedOutput)
	{
		this->obtainedOutput = obtainedOutput;
	}
	float crossEntropyFunction() {
		float sum = 0;
		for (int i = 0;i < nbOfInputs;i++)
		{
			sum += trueOutput[i] * log(obtainedOutput[i]) + (1 - trueOutput[i]) * log(1 - obtainedOutput[i]);
		}
		return (sum);
	}
	float crossEntropyFunctionDerivative(int index)
	{

	}
};

int main()
{
    int n = 10;
    cout << "Hello World!\n";
    cout << "Generating first layer with " << n << " neurons...\n";
    FullyConnectedLayer l1 = FullyConnectedLayer(n,10);
    FullyConnectedLayer l2 = FullyConnectedLayer(10,3);
    cout << "Generated all the layers";
    float input[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	float output[10] = { 0 };
	output = l1.calculateOutput(input);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
