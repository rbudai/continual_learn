#pragma once
#include "SigmoidNeuron.cpp"


class FullyConnectedLayer
{
	private: 
		SigmoidNeuron* list;

		int nrOfNeurons;
		int nrOfInputs;
	public:
		FullyConnectedLayer(int nrOfNeurons, int nrOfInputs)	
		{
			this->nrOfNeurons = nrOfNeurons;
			this->nrOfInputs = nrOfInputs;
			list = (SigmoidNeuron *)malloc(nrOfNeurons * sizeof(SigmoidNeuron));
			for (int i = 0; i < nrOfNeurons; i++)
			{
				list[i].setNrOfInputs(this->nrOfInputs);
				list[i].updateWeigthsRandomly();
			}
		}
		void DeleteLayer()
		{
			free(list);
		}
		SigmoidNeuron getNeuron(int index)
		{
			return(list[index]);
		}
		void printNeurons()
		{
			std::cout << "layer: \n";
			for (int i = 0; i < nrOfNeurons; i++)
			{
				std::cout << "Neuron: " << i << " ";
				list[i].toString();
				
			}
		}
		int getNrOfInputs()
		{
			return nrOfInputs;
		}
		int getNrOfNeurons()
		{
			return nrOfNeurons;
		}
		float* calculateOutput(float* input)
		{
			float* output;
			output = (float*)malloc(nrOfNeurons * sizeof(float));
			for (int i = 0;i < nrOfNeurons;i++)
			{
				output[i] = list[i].calculateActivation(input);
			}
			return(output);
		}
};

